from app import db
import os
import os.path as op
import datetime
from sqlalchemy.event import listens_for
from .config import MOVIES_PATH
from flask_admin import form
from sqlalchemy.orm import relationship
ROLE_USER = 0
ROLE_ADMIN = 1


# Create user model.
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(100))
    last_name = db.Column(db.String(100))
    email = db.Column(db.String(120), unique=True)
    password = db.Column(db.String(264))
    picture = db.Column(db.String(200))

    # Flask-Login integration
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    # Required for administrative interface
    def __unicode__(self):
        return self.username


class Movie(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    alias = db.Column(db.String(100))
    name = db.Column(db.String(200))
    year = db.Column(db.Integer)
    country = db.Column(db.String(200))
    genre = db.Column(db.String(200))
    director = db.Column(db.String(200))
    runtime = db.Column(db.String(200))
    country = db.Column(db.String(100))
    description = db.Column(db.Text)
    img = db.Column(db.String(200))
    trailer_file = db.Column(db.String(200))
    trailer_code = db.Column(db.String(250))
    file = db.Column(db.String(200))
    images = relationship("Image", backref="movie")

    def __repr__(self):
        return '%s' % unicode(self.name)


class Image(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode(64))
    path = db.Column(db.Unicode(128))
    movie_id = db.Column(db.Integer, db.ForeignKey('movie.id'))

    def __unicode__(self):
        return self.name


@listens_for(Image, 'after_delete')
def del_image(mapper, connection, target):
    if target.path:
        # Delete image
        try:
            os.remove(op.join(MOVIES_PATH, target.path))
        except OSError:
            pass

        # Delete thumbnail
        try:
            os.remove(op.join(MOVIES_PATH,
                              form.thumbgen_filename(target.path)))
        except OSError:
            pass


class Schedule(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    movie_id = db.Column(db.Integer, db.ForeignKey('movie.id'))
    movie = db.relationship(Movie)
    time = db.Column(db.DateTime)


class News(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Unicode(64))
    content = db.Column(db.UnicodeText)
    date = db.Column(db.DateTime)
    publish = db.Column(db.Boolean, default=False)


class Chat(db.Model):
     id = db.Column(db.Integer, primary_key=True)
     user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
     user = db.relationship(User, backref=db.backref('chat', lazy='dynamic'))
     message = db.Column(db.Text)
     time = db.Column(db.DateTime)