import os
import os.path as op
from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore

SECRET_KEY = '*A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

basedir = os.path.abspath(os.path.dirname(__file__))
FILE_PATH = op.join(basedir, 'files')
MOVIES_PATH = op.join(FILE_PATH, 'movies')

SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_lsrepository')

LANGUAGES = {
    'en': 'English',
    'ru': 'Russian'
}

# -------- Oauth config ---------
GOOGLE_ID = "635724711601-mvp04c0o1c2pooee36f7abbvbnfqqoue.apps.googleusercontent.com"
GOOGLE_SECRET = "GEytYZkBiUHN2uGt4mqEfTaK"

GOOGLE_KWARGS = {
    'consumer_key': GOOGLE_ID,
    'consumer_secret': GOOGLE_SECRET,
    'request_token_params': {
        'scope': 'email'
    },
    'base_url': 'https://www.googleapis.com/oauth2/v1/',
    'request_token_url': None,
    'access_token_method': 'POST',
    'access_token_url': 'https://accounts.google.com/o/oauth2/token',
    'authorize_url': 'https://accounts.google.com/o/oauth2/auth',
}

FACEBOOK_APP_ID = '707044666059436'
FACEBOOK_APP_SECRET = '54e5822eec4356987cd6eda48c708d30'

FACEBOOK_KWARGS = {
    'consumer_key': FACEBOOK_APP_ID,
    'consumer_secret': FACEBOOK_APP_SECRET,
    'request_token_params': {'scope': 'email'},
    'base_url': 'https://graph.facebook.com',
    'request_token_url': None,
    'access_token_url': '/oauth/access_token',
    'access_token_method': 'GET',
    'authorize_url': 'https://www.facebook.com/dialog/oauth'
}

VK_ID = "4813174"
VK_SECRET = "90nIQ7qksJKxOlL7Sz0l"

VK_KWARGS = {
    'consumer_key': VK_ID,
    'consumer_secret': VK_SECRET,
    'request_token_params': {'scope': 'email'},
    'base_url': 'https://api.vk.com/method',
    'request_token_url': None,
    'access_token_url': '/oauth/access_token',
    'access_token_method': 'GET',
    'authorize_url': 'https://oauth.vk.com/authorize'
}

# https://github.com/youtube/api-samples/blob/master/python/comment_handling.py

client_secrets_file_name = "client_secrets.json"

CLIENT_SECRETS_FILE = os.path.abspath(os.path.join(os.path.dirname(__file__), client_secrets_file_name))

# This OAuth 2.0 access scope allows for full read/write access to the
# authenticated user's account and requires requests to use an SSL connection.
YOUTUBE_READ_WRITE_SSL_SCOPE = "https://www.googleapis.com/auth/youtube.force-ssl"
YOUTUBE_READ_WRITE_SCOPE = "https://www.googleapis.com/auth/youtube"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

YOUTUBE_KWARGS = {
    'consumer_key': GOOGLE_ID,
    'consumer_secret': GOOGLE_SECRET,
    'request_token_params': {
        'scope': YOUTUBE_READ_WRITE_SSL_SCOPE,
        # 'response_type': 'code',
        # 'access_type': 'offline'
    },
    'base_url': 'https://accounts.google.com/o/oauth2/auth',
    'access_token_method': 'POST',
    'access_token_url': 'https://accounts.google.com/o/oauth2/token',
    'authorize_url': 'https://accounts.google.com/o/oauth2/auth',
}

# This variable defines a message to display if the CLIENT_SECRETS_FILE is
# missing.
MISSING_CLIENT_SECRETS_MESSAGE = """
WARNING: Please configure OAuth 2.0
To make this sample run you will need to populate the client_secrets.json file
found at:
   %s
with information from the APIs Console
https://console.developers.google.com
For more information about the client_secrets.json file format, plea se visit:
https://developers.google.com/api-client-library/python/guide/aaa_client_secrets
""" % CLIENT_SECRETS_FILE

# -------------------------- END OAUTH -------------------------------

# ---------------------------- JSON ----------------------------------
JSON_SORT_KEYS = False
# --------------------------------------------------------------------

# ---------------------------- THUMBNAILS -----------------------------

MEDIA_FOLDER = op.join(FILE_PATH, 'media')
MEDIA_URL = '/media/'

# -----------------------------------------------------------------------

try:
    from local_config import *
except ImportError:
    pass

# ---------------------------- SCHEDULE -------------------------------
JOBS = []

SCHEDULER_VIEWS_ENABLED = True


SCHEDULER_JOBSTORES = {
    'default': SQLAlchemyJobStore(url=SQLALCHEMY_DATABASE_URI)
}

SCHEDULER_EXECUTORS = {
    'default': {'type': 'threadpool', 'max_workers': 20}
}

SCHEDULER_JOB_DEFAULTS = {
    'coalesce': False,
    'max_instances': 3
}

# ---------------------------------------------------------------------

