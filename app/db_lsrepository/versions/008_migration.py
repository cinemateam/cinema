from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
chat = Table('chat', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('user_id', Integer),
    Column('message', Text),
    Column('time', DateTime),
)

movie = Table('movie', pre_meta,
    Column('id', INTEGER, primary_key=True, nullable=False),
    Column('name', VARCHAR(length=200)),
    Column('genre', VARCHAR(length=200)),
    Column('director', TEXT),
    Column('runtime', VARCHAR(length=200)),
    Column('description', VARCHAR),
    Column('country', VARCHAR(length=100)),
)

movie = Table('movie', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('alias', String(length=100)),
    Column('name', String(length=200)),
    Column('genre', String(length=200)),
    Column('director', String(length=200)),
    Column('runtime', String(length=200)),
    Column('description', Text),
    Column('img', String(length=200)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['chat'].create()
    pre_meta.tables['movie'].columns['country'].drop()
    post_meta.tables['movie'].columns['alias'].create()
    post_meta.tables['movie'].columns['img'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['chat'].drop()
    pre_meta.tables['movie'].columns['country'].create()
    post_meta.tables['movie'].columns['alias'].drop()
    post_meta.tables['movie'].columns['img'].drop()
