from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
news = Table('news', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('title', Unicode(length=64)),
    Column('content', UnicodeText),
    Column('date', DateTime),
    Column('publish', Boolean, default=ColumnDefault(False)),
)

image = Table('image', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', Unicode(length=64)),
    Column('path', Unicode(length=128)),
    Column('movie_id', Integer),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['news'].create()
    post_meta.tables['image'].columns['movie_id'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['news'].drop()
    post_meta.tables['image'].columns['movie_id'].drop()
