from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
movie = Table('movie', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=200)),
    Column('genre', String(length=200)),
    Column('director', String(length=200)),
    Column('runtime', String(length=200)),
    Column('description', Text),
)

schedule = Table('schedule', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('movie_id', Integer),
    Column('time', DateTime),
)

user = Table('user', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('first_name', String(length=100)),
    Column('last_name', String(length=100)),
    Column('email', String(length=120)),
    Column('password', String(length=264)),
    Column('auth_link', String(length=264)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['movie'].create()
    post_meta.tables['schedule'].create()
    post_meta.tables['user'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['movie'].drop()
    post_meta.tables['schedule'].drop()
    post_meta.tables['user'].drop()
