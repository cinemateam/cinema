from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
movie = Table('movie', pre_meta,
    Column('id', INTEGER, primary_key=True, nullable=False),
    Column('name', VARCHAR(length=200)),
    Column('genre', VARCHAR(length=200)),
<<<<<<< HEAD
    Column('director', TEXT),
    Column('runtime', VARCHAR(length=200)),
    Column('description', VARCHAR),
    Column('img_big', VARCHAR(length=255)),
    Column('img_small', VARCHAR(length=255)),
    Column('trailer', VARCHAR(length=255)),
)

user = Table('user', pre_meta,
    Column('id', INTEGER, primary_key=True, nullable=False),
    Column('first_name', VARCHAR(length=100)),
    Column('last_name', VARCHAR(length=100)),
    Column('login', VARCHAR(length=80)),
    Column('email', VARCHAR(length=120)),
    Column('password', VARCHAR(length=264)),
)

user = Table('user', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('first_name', String(length=100)),
    Column('last_name', String(length=100)),
    Column('email', String(length=120)),
    Column('password', String(length=264)),
    Column('auth_link', String(length=264)),
=======
    Column('director', VARCHAR(length=200)),
    Column('runtime', VARCHAR(length=200)),
    Column('description', TEXT),
    Column('alias', VARCHAR(length=100)),
    Column('img_small', VARCHAR(length=200)),
)

movie = Table('movie', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('alias', String(length=100)),
    Column('name', String(length=200)),
    Column('genre', String(length=200)),
    Column('director', String(length=200)),
    Column('runtime', String(length=200)),
    Column('description', Text),
    Column('card', String(length=200)),
>>>>>>> 91f4468ba00b338ea3c22fa422aa10e8df38c13d
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
<<<<<<< HEAD
    pre_meta.tables['movie'].columns['img_big'].drop()
    pre_meta.tables['movie'].columns['img_small'].drop()
    pre_meta.tables['movie'].columns['trailer'].drop()
    pre_meta.tables['user'].columns['login'].drop()
    post_meta.tables['user'].columns['auth_link'].create()
=======
    pre_meta.tables['movie'].columns['img_small'].drop()
    post_meta.tables['movie'].columns['card'].create()
>>>>>>> 91f4468ba00b338ea3c22fa422aa10e8df38c13d


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
<<<<<<< HEAD
    pre_meta.tables['movie'].columns['img_big'].create()
    pre_meta.tables['movie'].columns['img_small'].create()
    pre_meta.tables['movie'].columns['trailer'].create()
    pre_meta.tables['user'].columns['login'].create()
    post_meta.tables['user'].columns['auth_link'].drop()
=======
    pre_meta.tables['movie'].columns['img_small'].create()
    post_meta.tables['movie'].columns['card'].drop()
>>>>>>> 91f4468ba00b338ea3c22fa422aa10e8df38c13d
