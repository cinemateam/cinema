# -*- coding: utf-8 -*-

import logging
import os

from werkzeug.routing import ValidationError

from ..config import MOVIES_PATH
from .. import scheduler, db
from flask import url_for, redirect, request
import flask_admin as admin
from flask_admin.contrib import sqla, fileadmin
from flask_admin.form import RenderTemplateWidget
from flask_admin.model.form import InlineFormAdmin
from flask_admin.contrib.sqla.form import InlineModelConverter
from flask_admin.contrib.sqla.fields import InlineModelFormList

import flask_login as login
from flask_admin import helpers, expose, form
from ..account.forms import LoginForm
from flask import flash

from ..models import Image
from flask.ext.admin.babel import gettext
from werkzeug import secure_filename
from wtforms import fields
from wtforms.widgets import TextArea

from ..models import Schedule
from ..config import SQLALCHEMY_DATABASE_URI

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


# Set up logger
log = logging.getLogger("flask-admin.sqla")

THUMBNAIL_WIDTH = 290
THUMBNAIL_HEIGHT = 120


# Переопределяем клсс ModelView, чтобы иметь возможноть передавать обработчик событитя
# on_model_change перед сохраненим модели
class CustomModelView(sqla.ModelView):
    def is_accessible(self):
        return login.current_user.is_authenticated

    # Model handlers
    def create_model(self, form):
        """
            Create model from form.
            :param form:
                Form instance
        """
        try:
            model = self.model()
            self._on_model_change(form, model, True)
            form.populate_obj(model)
            self.session.add(model)
            self.session.commit()
        except Exception as ex:
            if not self.handle_view_exception(ex):
                flash(gettext('Failed to create record. %(error)s', error=str(ex)), 'error')
                # log.exception('Failed to create record.')
            self.session.rollback()
            return False
        else:
            self.after_model_change(form, model, True)
        return True

    def update_model(self, form, model):
        """
            Update model from form.
            :param form:
                Form instance
            :param model:
                Model instance
        """
        try:
            self._on_model_change(form, model, False)
            form.populate_obj(model)
            self.session.commit()
        except Exception as ex:
            if not self.handle_view_exception(ex):
                flash(gettext('Failed to update record. %(error)s', error=str(ex)), 'error')
                # log.exception('Failed to update record.')
            self.session.rollback()
            return False
        else:
            self.after_model_change(form, model, False)
        return True


# Create customized model view class
class CModelView(sqla.ModelView):
    def is_accessible(self):
        return login.current_user.is_authenticated


# Для Новостей в админке подключаем html редактор
class CKTextAreaWidget(TextArea):
    def __call__(self, field, **kwargs):
        kwargs['class'] = 'ckeditor'
        return super(CKTextAreaWidget, self).__call__(field, **kwargs)


class CKTextAreaField(fields.TextAreaField):
    widget = CKTextAreaWidget()


# Новости
class NewsModelView(CustomModelView):
    form_overrides = dict(content=CKTextAreaField)

    create_template = 'ckedit.html'
    edit_template = 'ckedit.html'


# Расписание
class ScheduleModelView(CustomModelView):
    def after_model_change(self, form, model, is_created):
        schedule_id = model.id
        movie = form.movie.data
        time = form.time.data

        job_id = 'broadcast_' + str(movie.alias) + '_' + str(movie.id) + '_job'
        scheduler.add_job(job_id, broadcast, name='date', run_date=time, args=[movie, schedule_id, ],
                          replace_existing=True)


def broadcast(movie, schedule_id):
    engine = create_engine(SQLALCHEMY_DATABASE_URI, echo=True)
    Session = sessionmaker(bind=engine)
    session = Session()
    schedule = session.query(Schedule).get(schedule_id)
    if schedule:
        session.delete(schedule)
        session.commit()


# Встроеная в форму редактирования фильма форма для добавления извображений
# This widget uses custom template for inline field list
class CustomInlineFieldListWidget(RenderTemplateWidget):
    def __init__(self):
        super(CustomInlineFieldListWidget, self).__init__('field_list.html')


# This InlineModelFormList will use our custom widget and hide row controls
class CustomInlineModelFormList(InlineModelFormList):
    widget = CustomInlineFieldListWidget()

    def display_row_controls(self, field):
        return False

# Create custom InlineModelConverter and tell it to use our InlineModelFormList
class CustomInlineModelConverter(InlineModelConverter):
    inline_field_list_type = CustomInlineModelFormList


# # Customized inline form handler
class InlineModelForm(InlineFormAdmin):
    form_excluded_columns = ('path',)

    form_label = 'Image'

    def __init__(self):
        return super(InlineModelForm, self).__init__(Image)

    def postprocess_form(self, form_class):
        form_class.upload = fields.FileField(u'Image')
        return form_class

    def on_model_change(self, form, model):
        file_data = request.files.get(form.upload.name)
        path = os.path.join(MOVIES_PATH, model.movie.alias, 'imgs')
        if not os.path.exists(path):
            try:
                os.mkdir(path)
            except OSError:
                pass
        if file_data:
            model.path = secure_filename(file_data.filename)
            file_path = os.path.join(path, model.path)
            file_data.save(file_path)


class MovieModelView(CustomModelView):
    column_exclude_list = ['description', ]
    inline_model_form_converter = CustomInlineModelConverter

    inline_models = (InlineModelForm(),)

    form_extra_fields = {
        'img': form.ImageUploadField('Image', base_path=MOVIES_PATH, endpoint='main.get_movie_file',
                                     allowed_extensions=['jpg', 'png'], max_size=(1000, 500, True),
                                     thumbnail_size=(THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT, True)),
        # TODO: валидация файла или кода трейлера
        'trailer_file': form.FileUploadField('Trailer', base_path=MOVIES_PATH, allow_overwrite=True),
        'file': form.FileUploadField('file', base_path=MOVIES_PATH, allow_overwrite=True),
    }

    def on_model_change(self, form, model, is_create):
        form.img.relative_path = form.alias.data + '/'
        if form.trailer_file:
            form.trailer_file.relative_path = os.path.join(form.alias.data, 'trailer') + '/'
        form.file.relative_path = os.path.join(form.alias.data, 'file') + '/'

        if not is_create and model.alias and form.alias.data != model.alias:
            old = os.path.join(MOVIES_PATH, model.alias)
            new = os.path.join(MOVIES_PATH, form.alias.data)

            try:
                os.rename(old, new)
            except OSError:
                pass

        else:
            if is_create:
                path = os.path.join(MOVIES_PATH, form.alias.data)
                path_file = os.path.join(path, 'file')
                path_imgs = os.path.join(path, 'imgs')
                path_trailer = os.path.join(path, 'trailer')

                try:
                    os.mkdir(path)
                    os.mkdir(path_file)
                    os.mkdir(path_imgs)
                    os.mkdir(path_trailer)
                except OSError:
                    pass


class FileView(fileadmin.FileAdmin):
    allowed_extensions = ('jpg',)

    def is_accessible(self):
        return login.current_user.is_authenticated


# Create customized index view class that handles login & registration
class CAdminIndexView(admin.AdminIndexView):
    @expose('/')
    def index(self):
        if not login.current_user.is_authenticated:
            return redirect(url_for('.login_view'))
        return super(CAdminIndexView, self).index()

    @expose('/login/', methods=('GET', 'POST'))
    def login_view(self):
        # handle user login
        form = LoginForm(request.form)
        if helpers.validate_form_on_submit(form):
            user = form.get_user()
            login.login_user(user)

        if login.current_user.is_authenticated:
            return redirect(url_for('admin.index'))

        link = '<p>Don\'t have an account? <a href="' + url_for(
            'account.register_view') + '">Click here to register.</a></p>'
        self._template_args['form'] = form
        self._template_args['link'] = link
        return super(CAdminIndexView, self).index()

    @expose('/logout/')
    def logout_view(self):
        login.logout_user()
        return redirect(url_for('index'))
