from flask import Blueprint
from app.config import SQLALCHEMY_DATABASE_URI
from views import CModelView, MovieModelView, FileView, CAdminIndexView, NewsModelView, ScheduleModelView
from pytz import utc
from .. import scheduler

c_admin = Blueprint('c_admin', __name__, template_folder='templates')


def create_admin(app, filepath):
    from ..models import User, Movie, Schedule, News
    import flask_admin
    from app import db, app

    fadmin = flask_admin.Admin(app, '', index_view=CAdminIndexView(),
                               base_template='my_master.html',
                               template_mode='bootstrap3')

    fadmin.add_view(CModelView(User, db.session))
    fadmin.add_view(MovieModelView(Movie, db.session))
    fadmin.add_view(NewsModelView(News, db.session))
    fadmin.add_view(ScheduleModelView(Schedule, db.session))