#  coding: utf-8
from .. import db, google, facebook, vk
from ..models import User
from flask import url_for, redirect, request,  jsonify, session, render_template
from flask_oauthlib.client import OAuth, OAuthException
import flask_login as login
from . import account
from forms import RegistrationForm, LoginForm
from werkzeug.security import generate_password_hash


@account.route('/')
def index():
    context = {}
    if 'google_token' in session:
        me = google.get('userinfo')
        return render_template('account/index.html', data=me.data).encode('utf-8')
    else:
        form = LoginForm(request.form)
        context['form'] = form
    return render_template('account/form.html', **context)


def set_user(data):
     success = False
     if db.session.query(User).filter_by(email=data['email']).count() == 0:
        data['password'] = generate_password_hash(data['email'])
        form = RegistrationForm(data)
        if form.validate():
            user = User()
            form.populate_obj(user)
            db.session.add(user)
            db.session.commit()
            success = True
     else:
        form = LoginForm(data)
        if form.validate():
            user = form.get_user()
            login_user(user)
            login.login_user(user)
            success = True
     return success


def set_user_by_oauth(oauth_data):
    pass


@account.route('/g_login')
def google_login():
    return google.authorize(callback=url_for('account.google_authorized',  _external=True))


@account.route('/g_logout')
def google_logout():
    session.pop('google_token', None)
    return redirect(url_for('index'))


@account.route('/google/authorized')
def google_authorized():
    resp = google.authorized_response()
    if resp is None:
        return 'Access denied: reason=%s error=%s' % (
            request.args['error_reason'],
            request.args['error_description']
        )
    session['oauth_token'] = (resp['access_token'], '')
    me = google.get('userinfo')
    response_data = me.data

    user_data = {
        'first_name': response_data['name'],
        'last_name': response_data['family_name'],
        'picture': response_data['picture']
    }

    session['user_data'] = user_data

    # set_user(me.data)
    # return jsonify({"data": me.data})
    return render_template('account/success.html')


@account.route('/facebook/login')
def facebook_login():
    callback = url_for(
        'account.facebook_authorized',
        next=request.args.get('next') or request.referrer or None, _external=True
    )
    return facebook.authorize(callback=callback)


@account.route('/facebook/authorized')
def facebook_authorized():
    resp = facebook.authorized_response()
    if resp is None:
        return 'Access denied: reason=%s error=%s' % (
            request.args['error_reason'],
            request.args['error_description']
        )
    if isinstance(resp, OAuthException):
        return 'Access denied: %s' % resp.message

    session['oauth_token'] = (resp['access_token'], '')
    me = facebook.get('/me')
    user_data = {
        'first_name': me.data['first_name'],
        'last_name': me.data['last_name'],
        'picture': 'https://graph.facebook.com/' + me.data['id'] + '/picture'
    }
    session['user_data'] = user_data
    # return 'Logged in as id=%s name=%s redirect=%s' % \
    #     (me.data['id'], me.data['name'], request.args.get('next'))
    # return jsonify({"data": me.data})
    return render_template('account/success.html')


@google.tokengetter
def get_google_oauth_token():
    return session.get('oauth_token')


@facebook.tokengetter
def get_facebook_oauth_token():
    return session.get('oauth_token')


@vk.tokengetter
def get_vk_oauth_token():
    return session.get('oauth_token')


@account.route('/vk/login')
def vk_login():
    callback = url_for(
        'account.vk_authorized',
        next=request.args.get('next') or request.referrer or None, _external=True
    )
    return vk.authorize(callback=callback)


@account.route('/vk/authorized')
def vk_authorized():
    resp = vk.authorized_response()
    if resp is None:
        return 'Access denied: reason=%s error=%s' % (
            request.args['error_reason'],
            request.args['error_description']
        )
    if isinstance(resp, OAuthException):
        return 'Access denied: %s' % resp.message
    access_token = resp.get('access_token', None)
    if access_token:
        session['oauth_token'] = (access_token, '')
        # me = vk.get('/method/users.get?params[user_ids]='+str(resp['user_id'])+'&params[fields]=photo_200&params[name_case]=Nom&params[v]=3.0')
        me = vk.get('/method/users.get', data={'user_ids':resp['user_id'], 'fields':'photo_50'} )
        r = me.data.get('response')
        if r:
            data = r[0]

            user_data = {
                'first_name': data['first_name'],
                'last_name':data['last_name'],
                'picture': data['photo_50']
            }
            session['user_data'] = user_data

            return render_template('account/success.html')


@account.route('/register/', methods=('GET', 'POST'))
def register_view():
    form = RegistrationForm(request.form,)
    if form.validate():
        user = User()

        form.populate_obj(user)
        # we hash the users password to avoid saving it as plaintext in the db,
        # remove to use plain text:
        user.password = generate_password_hash(form.password.data)

        db.session.add(user)
        db.session.commit()

        login.login_user(user)
        return redirect(url_for('.index'))

    link = '<p>Already have an account? <a href="' + url_for('.login') + '">Click here to log in.</a></p>'
    args = {'form': form, 'link': link}
    return render_template('account/registration.html', **args)


@account.route('/login', methods=['GET', 'POST'])
def login():
    # Here we use a class of some kind to represent and validate our
    # client-side form data. For example, WTForms is a library that will
    # handle this for us, and we use a custom LoginForm to validate.
    form = LoginForm()
    if form.validate():
        user = form.get_user()
        login_user(user)

        flask.flash('Logged in successfully.')

        next = request.args.get('next')
        # next_is_valid should check if the user has valid
        # permission to access the `next` url
        if not next_is_valid(next):
            return flask.abort(400)

        return flask.redirect(next or url_for('index'))
    return render_template('form.html', form=form)


@account.route('/logout/')
def logout_view(self):
    login.logout_user()
    return redirect(url_for('.index'))
