var Timeline = function (element, now) {
    var $timeline = element || document.getElementById("timeline");
    var cardWidth = 300;
    var htmlMargin = 0;
    var i = 30;
    var now = now || new Date;
    var minutes_img_width = 10;

    //var html_ms = function () {
    //    //var html_minutes = parseInt($timeline.offsetWidth-2*cardWidth / minutes_img_width);
    //    var html_minutes = parseInt($timeline.offsetWidth / minutes_img_width);
    //    return html_minutes * 60 * 1000;
    //}();

    var minutesToMs = function (minutes) {
        return minutes * 60 * 1000;
    }

    var msToMinutes = function (ms) {
        return ms / 1000 / 60;
    }

    //$timeline.style.width = msToHtmlWidth(html_ms) + 'px';

    var msToHtmlWidth = function (ms) {
        var minutes = msToMinutes(ms);
        var width = minutes * minutes_img_width;
        return width;
    }

    var setHtmlMargin = function(value){
        if(htmlMargin>0){
            htmlMargin +=  value;
        }else{
            htmlMargin =  value;
        }
    }

    var html_ms =minutesToMs(7* 24*60) ; // 7 дней переводим в милисекунды
    var halfScreenMs = minutesToMs(parseInt(window.screen.width/2/minutes_img_width));
    var _getStartTime = function(now){
           //return now - html_ms / 2 + cardWidth;
           var d = new Date(now);
           //return now - html_ms / 2 ;
           // выставляем начальное время на таймлайне как 00:00 сегодня
           var clear_date = new Date((d.getMonth()+1)+'.'+(d.getDate())+'.'+d.getFullYear()).getTime();
           var startTime = clear_date-halfScreenMs;
           setHtmlMargin(msToHtmlWidth(parseInt(d.getTime()-clear_date)));
           return startTime;
    }

    var startTimeMs = _getStartTime(now);

    var zeromize = function (time_el) {
        if (time_el < 10) time_el = "0" + time_el;
        return time_el;
    }

    var createTimeElement = function (date) {
        var day = date.getDate();
        var month = date.getMonth()+1;
        var hour = date.getHours();
        var min = date.getMinutes();
        var e = document.createElement("div");
        var id = day+"_"+month+"_"+hour + "_" + min;
        var span = document.createElement("span");
        span.className = "col__time";
        var html =  zeromize(hour) + ":" + zeromize(min);

        span.innerHTML = html;
        e.id = id;
        e.className = "col";
        e.appendChild(span);

        if(hour == 0 && min == 0 ){
            var span_date = document.createElement("span");
            span_date.className = "col__date";
            var date_html = zeromize(day)+"."+zeromize(month);
            span_date.innerHTML = date_html;
            e.appendChild(span_date);
        }

        return e;
    }


    function getMinutes(time) {
        var date = new Date(time);
        return date.getMinutes();
    }

    var floorMinutesMargin = function (minutes) {
        var margin = null;
        if (minutes < i) {
            margin = minutes;
        } else {
            margin = minutes % i;
        }

        setHtmlMargin(margin * minutes_img_width);
        return margin
    }

    var ceilByMinutes = function (time) {
        var minutes = getMinutes(time);
        var margin = minutesToMs(ceilMinutesMargin(minutes));
        //_this.htmlMargin = msToHtmlWidth(margin)
        var res = time + margin;
        return res;
    }

    var floorByMinutes = function (time) {
        var minutes = getMinutes(time);
        var margin = minutesToMs(floorMinutesMargin(minutes));
        var res = time - margin;
        return res;
    }

    var ceilMinutesMargin = function (minutes) {
        var margin = null;
        if (minutes < i) {
            margin = i - minutes;
        } else {
            margin = 60 - minutes;
        }
        setHtmlMargin(margin * minutes_img_width);
        return margin;
    }

    var renderCard = function (timestamp, card) {
        var time = parseInt(timestamp);
        var time_floor = floorByMinutes(time);
        var date = new Date(time_floor);
        var real_date = new Date(time);
        var h =  zeromize(real_date.getHours());
        var m = zeromize(real_date.getMinutes());
        
        var margin = msToHtmlWidth(time - time_floor);
        // var $card = $(card).css('margin-left', margin).append('<span class="showtime">'+h+' : '+m+'</span>');
        var $card = $(card).css('margin-left', margin);
        $('#'+real_date.getDate()+"_"+(real_date.getMonth()+1)+"_" + date.getHours()+ "_" + date.getMinutes()).addClass('has-card').append($card);
    }

    var getCard = function (data) {
        var htmlStyle = '';        
        return "<div id='"+data.id+"' data-movie_id='" + data.movie_id + "' class='card'>" +
            "<div class='img'><img width="+cardWidth+"' height='150' src='" + data.image_thmb + "'></div>" +
            "<div class='info' style='"+htmlStyle+"'>" + data.name + "</div>" +
            "</div>";
    }
    
    var renderCards = function (data) {
        var card = "";
        data = data['movies'];
        data.forEach(function (item) {
            card = getCard(item);
            renderCard(item.time*1000, card);
        });
    }
    


    return {
        getStartTime:function(){
            return startTimeMs;
        },
        getEndTime:function(){
            return  startTimeMs+html_ms
        },
        setStartTime:function(now){
            startTimeMs = _getStartTime(now);
        },
        getHtmlMargin: function(){
            return htmlMargin;
        },
        getWidth:function(){
            return msToHtmlWidth(html_ms)
        },
        clearHtml: function(){
          $timeline.innerHTML = "";
        },
        loadCards: function(handler){
            return $.getJSON("/cards",
                    function (data) {
                        $.when(renderCards(data)).then(function(){  handler(data)  });
                    });
        },
        render: function () {
            var fstartTimeMs = floorByMinutes(startTimeMs);

            //setHtmlLeftMargin();
            var finishTimeMs = fstartTimeMs + html_ms;
            var count = 0;
            for (var i = fstartTimeMs; i < finishTimeMs; i += 1800000) { //шаг 30 мин
                var date = new Date(i);
                var e = createTimeElement(date);
                $timeline.appendChild(e);
                count++;
            }
        }
    }
};