var slider = (function () {
    var $slider = document.getElementById('slider-content');
    var container = document.getElementById('slider-content');
    var images = document.getElementsByClassName('slide__images')[0];
    var $sliderTemplate = $("#slide-template").html();
    var $sliderImagesTemplate = $("#slide__images-template").html();

    return {
        getElement: function(){
            return $slider;
        },
        // "движок" слайдера
        move: function (value) {
            var value = "translateX(" + value + "px)";
            var css = {
                'transform': value,
                '-webkit-transform': value,
                '-moz-transform': value,
                '-ms-transform': value,
                '-o-transform': value
            }

            jQuery.extend($slider.style, css);
        },
        initButtonHandlers: function () {
            if(!line_margin){
                  var line_margin = getLeft(document.getElementById('line'));
            }

            var _this = this;
             // Обработчики кнопок "влево", "вправо" у слайдера
            var $left_btn = document.getElementById('left-arrow');
            var $right_btn = document.getElementById('right-arrow');

            // Обработчик меню для каждого слайда (фильма)
            var menu_links = document.getElementsByClassName('slide__menu__link');
            for (var i = 0; i < menu_links.length; i++) {
                menu_links[i].addEventListener('click', function (e) {
                    e.preventDefault();
                    var $this =  $(this);
                    var dataset = this.dataset;
                    var targetClassName = 'slide__' + dataset.target;

                    var $slide = $this.closest('.slider__slide');
                    var movie_id = $slide[0].id.split('_')[1];
                    var href = this.href+'?movie_id='+movie_id;
                    var $target = $slide.find('.'+targetClassName);

                    if (dataset.loaded == '0') {
                        $.getJSON(href, function (data) {
                            if(targetClassName=='slide__images'){
                                _this.renderMenuContent(data, $target[0], targetClassName);
                                setTimeout(function(){
                                      $('.image-gallery').lightSlider({
                                            gallery: true,
                                            item: 1,
                                            thumbItem: data['data'].length,
                                            slideMargin: 0,
                                            thumbMargin: 10,
                                            currentPagerPosition: 'top',
                                            speed: 600,
                                            auto: true,
                                            loop: true,
                                            autoWidth: true,
                                            verticalHeight: 450,
                                            pause: 4000,
                                            controls: false,
                                            onSliderLoad: function () {
                                                $('.image-gallery').removeClass('cS-hidden');
                                            }
                                        })
                                },300);
                            }else{
                                _this.renderMenuContent(data, $target[0], targetClassName);
                            }
                            dataset.loaded = '1';
                        });
                    }else{
                         if(targetClassName=='slide__trailer' && dataset.trailer_code){
                             var data = {'data':{'trailer_code':dataset.trailer_code}}
                              _this.renderMenuContent(data, $target[0], targetClassName);
                         }
                    }

                    $slide.find('.slide__content > div').each(function () {
                        var $$this = $(this);
                        if (this.className == targetClassName) {
                           $$this.show();
                        } else {
                           $$this.hide();
                        }
                    });
                });
            }

            $right_btn.addEventListener('click', function () {
                var active_cards = document.getElementsByClassName('active');
                if(active_cards.length>0) {
                    var $active = active_cards[0];
                    moveCardBySlide($active, "next");
                }
                //try {
                //    var next = active.nextElementSibling;
                //    var right = getLeft($slider) - getLeft(next);
                //} catch (e) {
                //    return false;
                //}


                //if (next) {
                    //moveCardBySlide(next);
                    //
                    //addClass(next, 'active');
                    //_this.move(right);
                //}
            });

            $left_btn.addEventListener('click', function () {
                var active_cards = document.getElementsByClassName('active');
                if(active_cards.length>0) {
                    var $active = active_cards[0];
                    moveCardBySlide($active, "prev");
                }

                //try {
                //    var prev = active.previousElementSibling;
                //    var left = getLeft($slider) - getLeft(prev);
                //} catch (e) {
                //    return false;
                //}

                //if (prev) {
                //    moveCardBySlide(prev);
                //    removeClass(active, 'active');
                //    addClass(prev, 'active');
                //    _this.move(left);
                //}
            });

            var moveCardBySlide = function(card,direct){
                   //var data_id = slide.dataset.card_id;
                   //var card = document.getElementById(data_id);
                   var cardToSelect = null;
                   if(direct == 'next'){
                        var $nextEl = $(card.parentElement).nextAll('.has-card');
                        if($nextEl.length>0){
                            cardToSelect = $nextEl[0].querySelector('.card');
                        }
                   }else{
                        var $prevEl = $(card.parentElement).prevAll('.has-card');
                        if($prevEl.length>0){
                            cardToSelect = $prevEl[0].querySelector('.card');
                        }
                   }
                   if(cardToSelect){
                        removeClass(card, 'active');
                        //var $selectedSlide = document.getElementById('slide_'+cardToSelect.dataset.movie_id);
                        addClass(cardToSelect , 'active');
                        cardToSelect.click();
                   }
            }
        },
        clearHtml: function(){
            container.innerHTML = '';
        },
        renderMenuContent: function(data, target,targetClassName){
            var temp = $('#'+targetClassName+'-template').html();
            var template = Handlebars.compile(temp);

            data = data['data'];
            var html = template(data)
            target.insertAdjacentHTML('beforeEnd', html);
        },
        render: function (data) {
            var template = Handlebars.compile($sliderTemplate);

            //Compile the template​
            data = data['movies'];
            var movie_ids = [];
            var sortedData = [];
            data.forEach(function(item, i, arr) {
                if(movie_ids.indexOf(item.movie_id) == -1){
                    movie_ids.push(item.movie_id);
                    sortedData.push(item);
                }
            });
            container.insertAdjacentHTML('beforeEnd', template(sortedData));
            var $slide = $('.slider__slide');
            $slide.width(document.documentElement.clientWidth);

            this.initButtonHandlers();
            $(window).trigger('resize')
        }
    }
}())