var schedule = (function(){
    var container = document.getElementById('main-schedule');
    var templateScript = $("#schedule-template").html();

    return {
        initButtonHandlers: function () {
             var $movie_links = document.getElementsByClassName('clickable');
                for(var i=0;i<$movie_links.length;i++){
                    $movie_links[i].addEventListener('click', function () {
                        var card_id = this.dataset.id;
                        var $card = document.getElementById(''+card_id+'');
                        $($card).click();
                    });
                }
        },
        loadAndDisplay: function (cards_data) {
            var _this = this;
            $.getJSON("main_schedule", function (data) {
                 _this.render(data);
                 _this.initButtonHandlers();
            });
        },
        render: function (data) {
            data = data['data'];
            //Compile the template​
            var template = Handlebars.compile(templateScript);
            var html = template(data);
            container.innerHTML = html;
            $(window).trigger('resize')
        }
    }
}())