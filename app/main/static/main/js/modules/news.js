var news = (function(modules){
    var container = document.getElementById('news');
    var templateScript = $("#news-template").html();

    return {
        loadAndDisplay: function(){
            var _this = this;
            $.getJSON("news", function (data) {
                _this.render(data);
            });
        },
        render: function (data) {
            data = data['news'];
            //Compile the template​
            var template = Handlebars.compile(templateScript);
            container.insertAdjacentHTML('beforeEnd', template(data));
        }
    }
}())
