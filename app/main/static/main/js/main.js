function digitalWatch() {
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    if (hours < 10) hours = "0" + hours;
    if (minutes < 10) minutes = "0" + minutes;
    if (seconds < 10) seconds = "0" + seconds;
    var sec = "<span class=\"time-dots\">:</span>" + seconds;
    document.getElementById("digital_watch").innerHTML = hours + "<span class=\"time-dots\">:</span>" + minutes;
    setTimeout("digitalWatch()", 1000);
}

$(function () {
    var $timeline = document.getElementById('timeline');
    var $line = document.getElementById('line');
    var $close = document.getElementsByClassName('close');

    //var pep_width = window.screen.width * 4; // ширина таймлайна - 4 экранов
    //var pep_width = 110000;
    //$timeline.style.width = pep_width;
    var halfScreen = window.screen.width/2;
    //$timeline.style.marginLeft = -((pep_width / 2)-halfScreen);

    var now = new Date();
    // digitalWatch();
    
    var $middle_content_els = document.getElementsByClassName('middle-content');
    
    var clear_middle_block_class = function(){
         Array.prototype.forEach.call($middle_content_els, function(el, i){
            if(hasClass(el,'show')){
                removeClass(el,'show');
            }
        });
    }

    $('body').on('click','.close', function(){
        var $middle_content = $(this).closest('.middle-content');
            $middle_content.removeClass('show');
    });

    var timeline = new Timeline($timeline, now);
    timeline.render();
    $line.setAttribute('data-now', now.getTime());

    // загружаем список фильмов и отображаем их на таймлайне и в слайдере
    var data_len;
    var callback = function(data){
        slider.render(data);
        var st = timeline.getStartTime();
        var $date_start = document.getElementById('date-start');
        var $date_end = document.getElementById('date-end');
        schedule.loadAndDisplay();
    }
    
    timeline.loadCards(callback);
    news.loadAndDisplay();

    line_margin = getLeft($line);
    var left_btn_opacity = 0;
    var left_border = -timeline.getWidth()+window.screen.width;
    
    // "джижок" таймлайна
    $($timeline).pep({
        axis: 'x',
        constrainTo: [0, 0, 0, left_border],
        cssEaseDuration: 750,
        cssEaseString: 'cubic-bezier(0.215, 0.610, 0.355, 1.000)',
        elementsWithInteraction: '.card',
        initiate: function (ev, obj) {},
        start: function (ev, obj) {},
        stop: function (ev, obj) {},
        rest: function (ev, obj) {}
    });

    var pep = $.pep.peps[0];
    pep.$el.css(pep.getCSSEaseHash());
    pep.moveToUsingTransforms(-timeline.getHtmlMargin(), 0);

    var $slider_el = document.getElementById('slider');
    var slide_left = 0;
    // при клике на карточку - сдвигаем ее к центру таймлайна
    // слайдер прокручиваем до информации о фильме из карточки
    $('body').on('click', '.card', function () {
        clear_middle_block_class();
        addClass($slider_el, 'show');
        $(window).trigger('resize');
        var left = getLeft(this);
        var dx = getLeft($line) - left;
        //var dx = left-line_margin;
        var id = this.dataset.movie_id;
        var $slide = document.getElementById('slide_'+id);
        var active= document.getElementsByClassName('active');
        if(active.length>0){
            var $active = active[0];
            removeClass($active,'active');
        }
        addClass(this,'active');
        var left = getLeft($slide);
            slide_left = slide_left-left;
            slider.move(slide_left);
            pep.moveToUsingTransforms(dx, 0);
    });

    var $menu__news = document.getElementById('menu__news');
    var $news = document.getElementById('news');
    $menu__news.addEventListener('click', function(e){
        e.preventDefault();
        clear_middle_block_class();
        addClass($news,'show');
        $(window).trigger('resize');
    });
    
    for(var i=0; i<$close.length; i++){
        $close[i].addEventListener('click',function(){
            clear_middle_block_class();
        });
    }

});