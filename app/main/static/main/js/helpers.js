var isoweekdays = {
    1:'Понедельник',
    2:'Вторник',
    3:'Среда',
    4:'Четверг',
    5:'Пятница',
    6:'Суббота',
    7:'Воскресенье'
}

Handlebars.registerHelper('list_schedule', function (items) {
    var out = "";
    items.forEach(function (item, i) {
        out += "<div class='date'><span class='month'>"+item['list']['date']+"</span> <span class='day'>" + isoweekdays[item['day']] + '</span>';
        item['list']['schedule'].forEach(function (sched, i) {
            //if(document.getElementById(''+sched[1]+'')){
            //    var clasHtml = "class='clickable'"
            //}else{
            //    var clasHtml = ""
            //}

            out += '<p data-id="'+sched[1]+'" class="clickable"><span >' + sched[0] + '</span><span class="movie-name">' + sched[2] + '</span></p>'
        });
        out += "</div>"
    });

    return out;
});
