function getDecimal(num) {
    var str = "" + num;
    var zeroPos = str.indexOf(".");
    if (zeroPos == -1) return 0;
    str = str.slice(zeroPos);
    return str;
}
function getLeft(object) {
    var rect = object.getBoundingClientRect();
    var left = rect.left + document.body.scrollLeft;
    return left;
}
function hasClass(el,className){
    if (el.classList)
      return el.classList.contains(className);
    else
      return new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
}
function addClass(el, className) {
    if (el.classList){
        if(el.classList.contains(className)) return;
        el.classList.add(className);
    }else{
        if(el.className.indexOf(className) != -1) return;
        el.className += ' ' + className;
    }
}
function removeClass(el, className) {
    if (el.classList) {
        if(!el.classList.contains(className)) return;
        el.classList.remove(className);
    }
    else {
        if(el.className.indexOf(className) == -1) return;
        el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
    }
}
function isHidden(el) {
    return (el.offsetParent === null)
}