import pprint
import os
import datetime
import time
from . import main
from ..models import Schedule, News, Movie
from ..config import MOVIES_PATH, MEDIA_FOLDER
from .. import thumb
from flask import url_for
from flask import request, render_template, get_template_attribute, jsonify
from sqlalchemy import and_, asc
from collections import OrderedDict

from flask import send_from_directory

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


@main.route('/')
def index():
    return render_template('main/index.html')


@main.route('/news')
def get_news():
    news = News.query.filter(News.publish).order_by('-id')
    news_to_response = []
    for n in news:
        news_to_response.append({'id': n.id,
                                 'title': n.title,
                                 'content': n.content,
                                 'date': n.date})
    data = {'news': news_to_response}
    return jsonify(data)



@main.route('/cards')
def get_cards():
    # from models import Schedule
    # startTime = int(request.args.get('startTime'))
    # time_ms = int(request.args.get('time'))

    today = datetime.date.today()
    d = datetime.timedelta(days=7)
    last_date = today + d

    # today = datetime.datetime.fromtimestamp(startTime / 1e3)
    # d = datetime.timedelta(milliseconds=time_ms)
    # last_date = today + d
    schedule = Schedule.query.filter(and_(Schedule.time > today, Schedule.time < last_date)).order_by("time asc").all()
    movies = []
    for s in schedule:
        if s.movie.img:
            img = s.movie.img
        else:
            img = "default.jpg"
        m = {
            'id': s.id,
            'movie_id': s.movie.id,
            'name': s.movie.name,
            'genre': s.movie.genre,
            'director': s.movie.director,
            'runtime': s.movie.runtime,
            'description': s.movie.description,
            'time': time.mktime(s.time.timetuple()),
            'image_thmb': url_for('main.get_movie_thumbnail')+'?image_name='+img,
            'image': url_for('main.get_movie_file')+'?filename='+img,
        }

        if s.movie.trailer_code:
            m['trailer_code'] = s.movie.trailer_code
        elif s.movie.trailer_file:
            m['trailer_file'] = s.movie.trailer_file
        movies.append(m)
    data = {'movies': movies}
    return jsonify(data)

# @main.route('/trailer', methods=['GET'])
# def get_trailer():
#     movie_id = request.args.get('movie_id')
#     movie = Movie.query.get(movie_id)
#     return


@main.route('/main_schedule')
def get_main_schedule():
    today = datetime.date.today()
    d = datetime.timedelta(days=7)
    last_date = today + d
    schedule = Schedule.query.filter(and_(Schedule.time > today, Schedule.time < last_date)).order_by("time asc").all()
    main_schedule = OrderedDict()
    for s in schedule:
        day = str(s.time.isoweekday())
        if day in main_schedule.keys():
            main_schedule[day]['schedule'].append((s.time.strftime('%H:%M'), s.id, s.movie.name))
        else:
            main_schedule.update({day: {'date': s.time.strftime('%d.%m'), 'schedule': [(s.time.strftime('%H:%M'), s.id, s.movie.name)]}})
    data = [{'day': i, 'list': v} for i, v in main_schedule.items()]
    return jsonify({'data': data})


@main.errorhandler(404)
def page_not_found(e):
    return render_template("main/404.html")


@main.route('/movie_file', methods=['GET'])
def get_movie_file():
    filename = request.args.get('filename')
    return send_from_directory(MOVIES_PATH, filename)


@main.route('/img', methods=['GET'])
def get_movie_image():
    movie = request.args.get('movie')
    filename = request.args.get('filename')
    path = os.path.join(MOVIES_PATH, movie, 'imgs')
    return send_from_directory(path, filename)


@main.route('/imgs', methods=['GET'])
def get_movie_images_list():
    movie_id = request.args.get('movie_id')
    movie = Movie.query.get(movie_id)
    images = movie.images
    data = list()
    pprint.pprint(images)
    for i in images:
        # thumb = get_template_attribute('main/_thumb.html', 'thumb')
        path = os.path.join(MOVIES_PATH, movie.alias, 'imgs', i.path)
        img_url = url_for('main.get_movie_image', movie=movie.alias, filename=i.path)
        thmb = thumb.thumbnail(path, '50x50', crop='fit', quality=100)
        thmb2 = thumb.thumbnail(path, '1000x500', crop='fit', quality=100)
        if thmb:
            thmb_path, thmb_name = os.path.split(thmb)
            thmb_url = url_for('main.get_movie_image', movie=movie.alias, filename=thmb_name)
        if thmb2:
            thmb2_path, thmb2_name = os.path.split(thmb2)
            thmb2_url = url_for('main.get_movie_image', movie=movie.alias, filename=thmb2_name)
        data.append({'id': i.id, 'name': i.name, 'thmb2': thmb2_url, 'thumb': thmb_url})
    return jsonify({'data': data})


@main.route('/thumb', methods=['GET'])
def get_movie_thumbnail():
    image_name = request.args.get('image_name')
    image_name_dict = image_name.split('.')
    image_name_dict[-2] += '_thumb'
    thumb_name = '.'.join(image_name_dict)
    return send_from_directory(MOVIES_PATH, thumb_name)


# @main.route('/media/<regex("([\w\d_/-]+)?.(?:jpe?g|gif|png)"):filename>')
# def media_file(filename):
#     return send_from_directory(MEDIA_FOLDER, filename)

def utc_mktime(utc_tuple):
    """Returns number of seconds elapsed since epoch

    Note that no timezone are taken into consideration.

    utc tuple must be: (year, month, day, hour, minute, second)

    """

    if len(utc_tuple) == 6:
        utc_tuple += (0, 0, 0)
    return time.mktime(utc_tuple) - time.mktime((1970, 1, 1, 0, 0, 0, 0, 0, 0))