from subprocess import call

VBR="2500k"
FPS=30
QUAL="medium"

YOUTUBE_URL="rtmp://a.rtmp.youtube.com/live2"
KEY = "DShondin.umqe-h98r-y01m-avtp"

def ffmpeg_stream(source, qual=QUAL, fps=FPS,vbr=VBR):
    return call(["ffmpeg", "-i", source,"-deinterlace",'-vcodec','libx264', '-pix_fmt','yuv420p','-preset', qual,'-r',fps,'-g', str(fps*2,),
          '-b:v', vbr,'-acodec','libmp3lame','-ar', '44100', '-threads', '6', '-qscale','3','-b:a','712000','-bufsize', '512k',
          '-f ','flv', YOUTUBE_URL+'/'+KEY])