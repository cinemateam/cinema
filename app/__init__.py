from flask.ext.oauthlib.provider import OAuth2Provider
import os
from . import config
from flask.ext.sqlalchemy import SQLAlchemy
from flask_oauthlib.client import OAuth
from flask.ext.socketio import SocketIO
import flask_login as login
import logging
from flask import Flask
from flask_apscheduler import APScheduler
from flask.ext.thumbnails import Thumbnail

db = SQLAlchemy()
oauth = OAuth()
oauth2 = None
google = None
facebook = None
youtube_api = None
vk = None
thumb = None
socketio = SocketIO()
login_manager = login.LoginManager()
scheduler = APScheduler()
# logging.basicConfig(filename='log', level=logging.ERROR)


# Initialize flask-login
def init_login(app):
    login_manager = login.LoginManager()
    login_manager.init_app(app)
    from models import User

    # Create user loader function
    @login_manager.user_loader
    def load_user(user_id):
        return db.session.query(User).get(user_id)


def create_app(debug=False):
    global facebook
    global google
    global app
    global oauth2
    global youtube_api
    global vk
    global thumb

    app = Flask(__name__, static_folder='static')
    app.config.from_object(config)

    app.debug = debug

    db.init_app(app)
    oauth.init_app(app)
    oauth2 = OAuth2Provider(app)
    init_login(app)

    thumb = Thumbnail(app)

    g_kwargs = app.config.get('GOOGLE_KWARGS')
    google = oauth.remote_app(
        'google', **g_kwargs
    )

    fb_kwargs = app.config.get('FACEBOOK_KWARGS')
    facebook = oauth.remote_app(
        'facebook', **fb_kwargs
    )

    vk_kwargs = app.config.get('VK_KWARGS')
    vk = oauth.remote_app(
        'vk', **vk_kwargs
    )

    filepath = app.config.get('FILE_PATH')

    try:
        os.mkdir(filepath)
    except OSError:
        pass

    from main import main
    from admin import c_admin, create_admin
    from room import room
    from account import account

    app.register_blueprint(c_admin, url_prefix='/admin')
    app.register_blueprint(account, url_prefix='/account')
    app.register_blueprint(main, url_prefix='')
    app.register_blueprint(room, url_prefix='/room')

    create_admin(app, filepath)
    socketio.init_app(app)
    scheduler.init_app(app)
    scheduler.start()
    return app