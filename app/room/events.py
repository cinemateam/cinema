# coding: utf-8
from flask import session
from flask.ext.socketio import emit, join_room, leave_room
from .. import socketio

room = 1

@socketio.on('joined', namespace='/chat')
def joined(message):
    """Sent by clients when they enter a room.
    A status message is broadcast to all people in the room."""
    join_room(room)
    user_data = session.get('user_data')
    if user_data:
        emit('status', {'msg': user_data.get('first_name') + u' присоединился к чату.'}, room=room)


@socketio.on('text', namespace='/chat')
def left(message):
    """Sent by a client when the user entered a new message.
    The message is sent to all people in the room."""
    if len(message['msg'].strip()) == 0:
        return
    user_data = session.get('user_data')
    if user_data:
        emit('message', {'user': {'avatar': user_data.get('picture'),
                                  'name': user_data.get('first_name'),
                                  'last_name': user_data.get('last_name')},
             'msg': message['msg']}, room=room)


@socketio.on('left', namespace='/chat')
def left(message):
    """Sent by clients when they leave a room.
    A status message is broadcast to all people in the room."""
    user_data = session.get('user_data')
    leave_room(room)
    if user_data:
        emit('status', {'msg':  user_data.get('first_name') + u' покинул чат.'}, room=room)

