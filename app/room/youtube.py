import httplib2
import sys
import cgi
import pprint
from apiclient.discovery import build_from_document, build
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import run_flow
from ..config import YOUTUBE_READ_WRITE_SSL_SCOPE, CLIENT_SECRETS_FILE, MISSING_CLIENT_SECRETS_MESSAGE


# Authorize the request and store authorization credentials.
def get_authenticated_service(cmd_flags, credential_file):
    flow = flow_from_clientsecrets(CLIENT_SECRETS_FILE,
                                   scope=YOUTUBE_READ_WRITE_SSL_SCOPE, message=MISSING_CLIENT_SECRETS_MESSAGE)
    storage = Storage(credential_file)
    credentials = storage.get()
    if credentials is None or credentials.invalid:
        credentials = run_flow(flow, storage, cmd_flags)

    return build('youtube', 'v3', http=credentials.authorize(httplib2.Http()))


def get_live_chat_id(youtube):
    resource = youtube.liveBroadcasts()
    results = youtube.liveStreams().list(
        part="id,snippet",
        mine=True,
        maxResults=50
    ).execute()
    pprint.pprint(results)
    return results['items'][0]['snippet']['liveChatId']


def get_comment_threads(youtube, video_id):
    results = youtube.liveStreams().list(
        part="id,snippet",
        mine=True,
        maxResults=50
    ).execute()

    for item in results["items"]:
        comment = item["snippet"]["topLevelComment"]
        author = comment["snippet"]["authorDisplayName"]
        text = comment["snippet"]["textDisplay"]
        print "Comment by %s: %s" % (author, text)

    return results["items"]


# Call the API's commentThreads.list method to list the existing comment threads.
# def get_comment_threads(youtube, live_chat_id):
# print "--------------get comments-----------------"
#   results = youtube.liveChatMessages().list(
#     liveChatId=live_chat_id,
#     part="snippet",
#   ).execute()
#   print "######## comments ##########"
#   pprint.pprint(results['items'])
#   for item in results["items"]:
#     # author = item["snippet"]["authorDisplayName"]
#     text = item["snippet"]["displayMessage"]
#     print "Comment by : %s" % (text)
#   return results["items"]


# Call the API's comments.insert method to reply to a comment.
# (If the intention is to create a new to-level comment, commentThreads.insert
# method should be used instead.)
def insert_comment(youtube, liveChatId, text):
    insert_result = youtube.liveChatMessages().insert(
        part="snippet",
        body=dict(
            snippet=dict(
                liveChatId=liveChatId,
                type='textMessageEvent',
                textMessageDetails=dict(messageText=text)
            )
        )
    ).execute()
    pprint.pprint(insert_result)
    # author = insert_result["snippet"]["authorDisplayName"]
    text = insert_result["snippet"]["displayMessage"]
    print "###########3Replied to a comment for: %s" % (text)


# Call the API's comments.update method to update an existing comment.
def update_comment(youtube, comment):
    comment["snippet"]["textOriginal"] = 'updated'
    update_result = youtube.comments().update(
        part="snippet",
        body=comment
    ).execute()

    author = update_result["snippet"]["authorDisplayName"]
    text = update_result["snippet"]["textDisplay"]
    print "Updated comment for %s: %s" % (author, text)


# Call the API's comments.setModerationStatus method to set moderation status of an
# existing comment.
def set_moderation_status(youtube, comment):
    youtube.comments().setModerationStatus(
        id=comment["id"],
        moderationStatus="published"
    ).execute()

    print "%s moderated succesfully" % (comment["id"])


# Call the API's comments.markAsSpam method to mark an existing comment as spam.
def mark_as_spam(youtube, comment):
    youtube.comments().markAsSpam(
        id=comment["id"]
    ).execute()

    print "%s marked as spam succesfully" % (comment["id"])


# Call the API's comments.delete method to delete an existing comment.
def delete_comment(youtube, comment):
    youtube.comments().delete(
        id=comment["id"]
    ).execute()

    print "%s deleted succesfully" % (comment["id"])