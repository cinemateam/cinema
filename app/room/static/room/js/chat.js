
            var socket;

            //var render_template = function(data){return '<div class="comment"><div class="comment__user"><img class="user__avatar" src="'+data.user.avatar+'"></div><div class="comment__message"><header><span class="username">'+data.user.name+': </span></header><div class="message__body">'+data.msg+'</div></div></div>'};
            var render_template = function(data){return '<div class="comment"><div class="comment__user" title="'+data.user.name+'"><img alt="'+data.user.name+'" class="user__avatar" src="'+data.user.avatar+'"></div><div class="comment__message"><div class="message__body">'+data.msg+'</div></div></div>'};


            $(document).ready(function(){
            	var win = $(window);
                socket = io.connect('http://' + document.domain + ':' + location.port + '/chat');
                socket.on('connect', function() {
                    socket.emit('joined', {});
                });

                socket.on('status', function(data) {
                    var $chat = $('#chat');
                    $chat.html($chat.html()+'<div class="comment"><div  class="comment__message">' + data.msg + '</div></div>');
                    $chat.scrollTop($chat[0].scrollHeight);
                    win.trigger('resize');
                });

                socket.on('message', function(data) {
                    var $chat = $('#chat');
                    var html = render_template(data);
                    $chat.html($chat.html()+html);
                });

                $('#text').keypress(function(e) {
                    var $text = $('#text');
                    var code = e.keyCode || e.which;
                    if (code == 13) {
                        var text = $text.val();
                        if(text.trim().length==0) return;
                        $text.val('');
                        socket.emit('text', {msg: text});
                    }
                });
            });

            function isOverflowedY(element){
                return element.scrollHeight > element.clientHeight;
            }

            function leave_room() {
                socket.emit('left', {}, function() {
                    socket.disconnect();
                    // go back to the login page
                    window.location.href = "{{ url_for('main.index') }}";
                });
            }