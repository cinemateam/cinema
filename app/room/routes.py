from flask import session, redirect, url_for, render_template, request
from . import room


@room.route('/', methods=['GET', 'POST'])
def index():
    fullscreen = request.args.get('fullscreen')
    if 'oauth_token' in session:
       login = False
    else:
       login = True
    """"Login form to enter a room."""
    if fullscreen:
        return render_template('room/index_fullscreen.html', login=login)
    else:
        return render_template('room/index.html', login=login)


@room.route('/chat')
def chat():
    """Chat room. The user's name and room must be stored in
    the session."""
    name = session.get('name', '')
    room = session.get('room', '')
    if name == '' or room == '':
        return redirect(url_for('main.index'))
    return render_template('room/chat.html', name=name, room=room)