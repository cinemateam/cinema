$(function(){

	var win = $(window);
	// Full body scroll
	var isResizing = false;
	win.on(
		'resize',
		function()
		{
            $('.slider__slide').width(document.documentElement.clientWidth);
			if (!isResizing) {
				isResizing = true;
				var container = $('#full-page-container');
				// Temporarily make the container tiny so it doesn't influence the
				// calculation of the size of the document
				container.css(
					{
						'width': 1,
						'height': 1
					}
				);
				// Now make it the size of the window...
				container.css(
					{
						'width': win.width(),
						'height': win.height()
					}
				);
				isResizing = false;
				container.jScrollPane({contentWidth: '0px'});
			}
		}
	).trigger('resize');



	// IE calculates the width incorrectly first time round (it
	// doesn't count the space used by the native scrollbar) so
	// we re-trigger if necessary.
	if ($('#full-page-container').width() != win.width()) {
		win.trigger('resize');
	}
})