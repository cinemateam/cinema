from app import create_app, socketio

app = create_app(debug=True)

if __name__ == '__main__':
    # app.run(host='10.0.0.86')
    socketio.run(app)